/********************
 A'Brea Ratchford
 abrear
 Lab 5
 Lab Section: 003
 TA: Nushrat Humaira
********************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

/*Create a deck of cards of size 52 (hint this should be an array) and
 *initialize the deck*/
//Initialize value and make a array of 52
//Write a for loop to create a deck of cards
  int i;
  int x;
  int r = 0;
  Card deck[52];

  for(x = 0; x <= 3; x++){
    for(i = 2; i <= 14; i++){
       deck[r].value = i;
       deck[r].suit = static_cast<Suit>(x);
       r++;
    }
  }

/*After the deck is created and initialzed we use the funtion random_shuffle()
  to determine the parameters to pass in.*/
  random_shuffle(&deck[0], &deck[52], myrandom);
  //random_shuffle(deck, deck[52]);

/*Build a hand of 5 cards from the first five cards of the deck created
 *above*/
  Card play[5] = {deck[0],deck[1],deck[2],deck[3],deck[4]};

/*Sort the cards, from the deck that you built,
 or from the hand that you built.*/
  sort(&deck[0], &deck[52], suit_order);

/*Now print the hand below. You will use the functions get_card_name and
 *get_suit_code */
//Write a for loop to print out your hand below
/*Make sure you call the functions get_card_name and
get_suit_code within the loop*/
  int j;

  for(j = 0; j < 5; j++) {
    cout << setw(10) << right;
    cout << get_card_name(play[j]);
    cout << get_suit_code(play[j]) << endl;
  }
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
//Write an if statement to tell what the suit and value
//If less than return true
//Other than that return false
  if(lhs.suit < rhs.suit){
    return true;
  }
  else if(lhs.suit == rhs.suit){
    if(lhs.value < rhs.value) {
        return true;
    }
  }
  return false;
}

/*This function creates the suits and gives you the code to
 print out each suit symbol*/
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
/*This function creates the name of the card. It prints
 out the number of the card and state what suit it is from*/
  switch (c.value) {
    case 2:    return "2 of ";
    case 3:    return "3 of ";
    case 4:    return "4 of ";
    case 5:    return "5 of ";
    case 6:    return "6 of ";
    case 7:    return "7 of ";
    case 8:    return "8 of ";
    case 9:    return "9 of ";
    case 10:   return "10 of ";
    case 11:   return "Jack of ";
    case 12:   return "Queen of ";
    case 13:   return "King of ";
    case 14:   return "Ace of ";
    default:   return "";
  }
}
